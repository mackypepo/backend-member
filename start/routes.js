'use strict'
/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/
//Rest API 
const Database = use('Database')
const axios = require('axios')

/** @type {import('@adonisjs/framework/src/Env')} */
const Env = use('Env')

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')
const Logger = use('Logger')
var Pusher = require('pusher');
const xlxs = require('xlsx');

// Route.post('/staff/login' , 'StaffController.login')

Route.post('/v1/member/checkbalance' , 'MemberController.checkBalance')

Route.post('/v1/member/login' , 'MemberController.loginMember')

Route.post('/v1/member/checkwithdraw' , 'MemberController.getProfileForWithdraw')

Route.post('/v1/member/startufa' , 'MemberController.startUfa')

Route.post('/v1/member/hisdeposit' , 'MemberController.historyDeposit')

Route.post('/v1/member/hiswithdraw' , 'MemberController.historyWithdraw')

Route.post('/v1/member/createwithdraw' , 'MemberController.createWithdraw')

Route.post('/v1/member/createdeposit' , 'MemberController.createDeposit')

Route.post('/v1/member/getwithdraw' , 'MemberController.getWithdraw')

Route.post('/v1/member/updatewithdraw' , 'MemberController.updateWithdraw')

Route.post('/v1/member/getdeposit' , 'MemberController.getDeposit')

Route.post('/v1/member/canceldeposit' , 'MemberController.cancelDeposit')

Route.post('/v1/member/sendotp', 'MemberController.sendSMSOTP')

Route.post('/v1/member/verifyotp', 'MemberController.verifyOtp')

Route.get('/v1/member/bankall', 'MemberController.bankAll')

Route.get('/v1/member/chanelall', 'MemberController.chanelAll')

Route.post('/v1/member/addmember', 'MemberController.addMemberNoSms')

Route.post('/v1/member/sendmessage', 'MemberController.sendMessage')

Route.post('/v1/member/testaddmember', 'MemberController.testaddmember')

// Route.get('/v1/member/testpusher' , 'MemberController.pusherTrigger')

Route.get('/v1/testCheck' , 'MemberController.testCheck')

Route.get('/v2/member/noti' , 'MemberController.getNoti')

//Route.post('/v1/member/pwforgot' , 'MemberController.sendMessageForgot')

Route.post('/v1/member/getpfref' , 'MemberController.getProfileRef')


Route.post('/v2/member/getpfref' , 'MemberController.getProfileRef2')

Route.post('/v2/member/hist' , 'MemberController.historyAll')

// Wildcard Route
Route.any('*', () => {
  return 
})

