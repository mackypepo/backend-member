'use strict'

const Database = use('Database')
const axios = require('axios')
const Encryption = use('Encryption')
const Pusher = require('pusher');
const twilio = require('twilio');

/** @type {import('@adonisjs/framework/src/Env')} */
const Env = use('Env')

class MemberController {

  async checkBalance(request , response){
    const body = request._request_._original
    console.log(body)
    var dt = new Date();
    dt.setHours(dt.getHours()+7);
    const member = Database.table('MEMBER')
    const token = Database.table('TOKENS_MEMBER')
    const webs = Database.table('WEBSITE')
    const promo = Database.table('PROMOTIONS')
    let website = await webs.where('ID' , '1').first()
    const encrypted = Encryption.decrypt(body.token.substring(0,56))
    let data;
    let stt ;
    let a = await token.where({TOKEN: body.token}).first()
    let b = await promo
    let m = await member.where('MEMBER_USERNAME',encrypted).first();
    console.log(a)
    if(a){
      if(a.IS_REVOKED == 1){
        return {
          status : 401,
          message: 'token expired'
        }
      }else{
      let g = await axios({
        method: 'post',
        url: 'https://kraken.mrwed.cloud/partner/user/credit',
        headers: { 
          'Content-Type': 'application/json', 
          'x-api-key': '5333dcb9-dd3b-4fd9-896a-8e42ec8968a4'
        },
        data : {
            "agentUsername": website.USERNAME,
            "agentPassword": website.PASSWORD,
            "username": encrypted
        }
        })
          console.log(g.data)       
        if(g.data.status == 'success'){
          stt = 200
          data = g.data.current_credit

        }else{
          stt = 300
          data = 0
        }
        console.log(data)
      return {
        status : stt,
        data: data,
        promo : b,
        ph : m.PHONE,
        id : m.MEMBER_USERNAME
      }
    }
    }else{
      return {
        status : 400,
        data: 'error'
      }
    }
   
  }

  async getProfileForWithdraw(request , response){
    const body = request._request_._original
    console.log(body)
    var dt = new Date();
    dt.setHours(dt.getHours()+7);
    const member = Database.table('MEMBER')
    const bank = Database.table('BANK')
    const token = Database.table('TOKENS_MEMBER')
    const trxwd = Database.table('TRANSACTION_WITHDAW')
    const webs = Database.table('WEBSITE')
    let website = await webs.where('ID' , '1').first()
    const encrypted = Encryption.decrypt(body.token.substring(0,56))
    let data;
    let stt ;
    let a = await token.where({TOKEN: body.token}).first()
    console.log(a)
    if(a){
      if(a.IS_REVOKED == 1){
        return {
          status : 401,
          message: 'token expired'
        }
      }else{
        let m = await member.where('MEMBER_USERNAME',encrypted).first();
     
        console.log(m)
        let trxH = await trxwd.select('ID','REMARK','TRANSACTION_STATUS','BALANCE').whereIn('TRANSACTION_STATUS', ['pending','return','reject']).where('MEMBER_ID',m.ID).first();
        console.log(trxH)
        if(trxH){
            return {
              status : 202,
              data : trxH
          }
        }else{
      let g = await axios({
        method: 'post',
        url: 'https://kraken.mrwed.cloud/partner/user/credit',
        headers: { 
          'Content-Type': 'application/json', 
          'x-api-key': '5333dcb9-dd3b-4fd9-896a-8e42ec8968a4'
        },
        data : {
            "agentUsername": website.USERNAME,
            "agentPassword": website.PASSWORD,
            "username": encrypted
        }
        })
          console.log(g.data)       
        if(g.data.status == 'success'){
          stt = 200
          data = g.data.current_credit
          let m = await member.where('MEMBER_USERNAME',encrypted).first();
          let b = await bank.where('ID',m.BANK_ID).first();
          console.log(m)
          console.log(b)
          return {
            status : stt,
            profile : {
              firstname : m.MEMBERFIRSTNAME,
              lastname : m.MEMBERLASTNAME,
              bankacc : m.BANK_ACCOUNT_NUMBER,
              bankname : b.BANKNAME
            },
            data: data
          }
        }else{
          stt = 300
          data = 'ufabet error'
          return {
            status : stt,
            data: data
          }
        }
  
    }
  }
    }else{
      return {
        status : 400,
        data: 'error'
      }
    }
  
  }

  async startUfa(request , response){
    const body = request._request_._original
    console.log(body)
    //console.log(param)
    var dt = new Date();
    dt.setHours(dt.getHours()+7);
    const member = Database.table('MEMBER')
    const token = Database.table('TOKENS_MEMBER')
    const webs = Database.table('WEBSITE')
    let website = await webs.where('ID' , '1').first()
    var encrypted = Encryption.decrypt(body.token.substring(0,56))
    console.log(encrypted)
    var encrypted2 = Encryption.decrypt(body.token.substring(56,112))
    // var dxxx = param.params.id.substring(5,112)
    console.log(encrypted2)
    let data;
    let stt ;
    let a = await token.where({TOKEN: body.token}).first()
    console.log(a)
    if(a){
      if(a.IS_REVOKED == 1){
        return {
          status : 401,
          message: 'token expired'
        }
      }else{
      let g = await axios({
        method: 'post',
        url: 'https://kraken.mrwed.cloud/partner/auth/login',
        headers: { 
          'Content-Type': 'application/json', 
          'x-api-key': '5333dcb9-dd3b-4fd9-896a-8e42ec8968a4'
        },
        data : {
          "agentUsername": website.USERNAME,
          "agentPassword": website.PASSWORD,
          "username": encrypted,
          "password": encrypted2
        }
        })
          console.log(g.data)       
        if(g.data.status == 'success'){
          stt = 200
          data = g.data.gameUrl
        }else{
          let mm = 'พบปัญหากรุณาติดต่อทางไลน์'
          if(g.data.status == 'error'){
            if(g.data.errorType == 'VALIDATION_ERROR' && g.data.errorCode == 'INVALID_CREDENTIAL'){
              mm = 'รหัสผ่านลูกค้าไม่ถูกต้อง กรุณาตรวจสอบ'
            }else if(g.data.errorType == 'MAINTENANCE' && g.data.errorCode == 'AGENT_MAINTENANCE'){
              mm = 'ขณะนี้ระบบปิดปรับปรุง กรุณารอสักครู่'
            }else{
              mm = g.data.message 
            }              
          }
          stt = 300
          data = mm
        }
      return {
        status : stt,
        data: data
      }
    }
    }else{
      return {
        status : 404,
        data: 'error'
      }
    }
   
  }

  async historyDeposit(request , response){
    const body = request._request_._original
    console.log(body)
    var dt = new Date();
    dt.setHours(dt.getHours()+7);
    const member = Database.table('MEMBER')
    const token = Database.table('TOKENS_MEMBER')
    const trxdepo = Database.table('TRANSACTION_DEPOSIT')
    const webs = Database.table('WEBSITE')
    let website = await webs.where('ID' , '1').first()
    var encrypted = Encryption.decrypt(body.token.substring(0,56))
    console.log(encrypted)
    var encrypted2 = Encryption.decrypt(body.token.substring(56,112))
    // var dxxx = param.params.id.substring(5,112)
    console.log(encrypted2)
    let data;
    let stt ;
    let a = await token.where({TOKEN: body.token}).first()
    console.log(a)
    if(a){
      if(a.IS_REVOKED == 1){
        return {
          status : 401,
          message: 'token expired'
        }
      }else{
        let m = await member.where('MEMBER_USERNAME',encrypted).first()
        console.log(m)       
        if(m){
          let trx = await trxdepo.select('BALANCE' , 'CREATEDATE').where('MEMBER_ID',m.ID).where('TRANSACTION_STATUS','success').orderBy('CREATEDATE', 'desc')
          console.log(trx)
          stt = 200
          data = trx
        }else{
          stt = 300
          data = "error"
        }
      return {
        status : stt,
        data: data
      }
    }
    }else{
      return {
        status : 400,
        data: 'error'
      }
    }
  }

  async historyWithdraw(request , response){
    const body = request._request_._original
    console.log(body)
    var dt = new Date();
    dt.setHours(dt.getHours()+7);
    const member = Database.table('MEMBER')
    const token = Database.table('TOKENS_MEMBER')
    const trxdepo = Database.table('TRANSACTION_WITHDAW')
    const webs = Database.table('WEBSITE')
    let website = await webs.where('ID' , '1').first()
    var encrypted = Encryption.decrypt(body.token.substring(0,56))
    console.log(encrypted)
    var encrypted2 = Encryption.decrypt(body.token.substring(56,112))
    // var dxxx = param.params.id.substring(5,112)
    console.log(encrypted2)
    let data;
    let stt ;
    let a = await token.where({TOKEN: body.token}).first()
    console.log(a)
    if(a){
      if(a.IS_REVOKED == 1){
        return {
          status : 401,
          message: 'token expired'
        }
      }else{
        let m = await member.where('MEMBER_USERNAME',encrypted).first()
        console.log(m)       
        if(m){
          let trx = await trxdepo.select('BALANCE' , 'UPDATEDATE','TRANSACTION_STATUS','REMARK').where('MEMBER_ID',m.ID).where('TRANSACTION_STATUS','success').orderBy('UPDATEDATE', 'desc')
          console.log(trx)
          stt = 200
         data = trx
        }else{
          stt = 300
          data = "error"
        }
      return {
        status : stt,
        data: data
      }
    }
    }else{
      return {
        status : 400,
        data: 'error'
      }
    }
  }

  async createWithdraw(request , response){
    const body = request._request_._original
    console.log(body)
    var dt = new Date();
    dt.setHours(dt.getHours()+7);
    const bank = Database.table('BANK_OWNER')
    const member = Database.table('MEMBER')
    const token = Database.table('TOKENS_MEMBER')
    const trxwd = Database.table('TRANSACTION_WITHDAW')
    const webs = Database.table('WEBSITE')
    let website = await webs.where('ID' , '1').first()
    var encrypted = Encryption.decrypt(body.token.substring(0,56))
    console.log(encrypted)
    var encrypted2 = Encryption.decrypt(body.token.substring(56,112))
    // var dxxx = param.params.id.substring(5,112)
    console.log(encrypted2)
    let data;
    let stt ;
    let a = await token.where({TOKEN: body.token}).first()
    console.log(a)
    if(a){
      if(a.IS_REVOKED == 1){
        return {
          status : 401,
          message: 'token expired'
        }
      }else{
        let m = await member.where('MEMBER_USERNAME',encrypted).first()
        console.log(m)       
        if(m){
          let trxH = await trxwd.where('TRANSACTION_STATUS' , 'pending').where('MEMBER_ID',m.ID).first();
          console.log(trxH)
          if(trxH){
            return {
              status : 202,
              data: 'Transaction In Progress'
            }
          }else{
            let v = await bank.where('BANK_TYPE','withdraw').first();
            let r = (Math.random() + 1).toString(36).substring(5);
            console.log('test')
            console.log(website)
            console.log(m)
            let g = await axios({
              method: "post",
              url: "https://kraken.mrwed.cloud/partner/user/credit/del",
              headers: {
                "Content-Type": "application/json",
                "x-api-key": "5333dcb9-dd3b-4fd9-896a-8e42ec8968a4",
              },
              data: {
                agentUsername: website.USERNAME,
                agentPassword: website.PASSWORD,
                username: m.MEMBER_USERNAME,
                credit: body.balance,
                txId:m.MEMBER_USERNAME+"_"+r
              },
            });
            console.log(g.data);     
              if(g.data.status == 'success'){
                stt = 200
                data = "success"

                try{
                  var InsertTransaction_Withdraw = await trxwd
                  .insert({
                    "MEMBER_ID" : m.ID, 
                    "BANK_OWNER_ID" : v.ID, 
                    "TRANSACTION_STATUS" : 'pending', 
                    "BALANCE" : body.balance, 
                    "CREATEDATE" : dt, 
                    "UPDATEDATE" : dt,
                    "ACTION_BY":'',
                    "UFABET_STATUS":"pending"
                  })
                  if(InsertTransaction_Withdraw){
                    stt = 200
                    data = 'success'
                    let cv = await this.pusherTrigger();
                  }else{
                    stt = 300
                    data = 'error'
                  }            
                }catch(err){
                    console.error(err)
                }

              }else{
                stt = 300
                data = "ufa error"
              }
        }
        }else{
          stt = 300
          data = "error"
        }
      return {
        status : stt,
        data: data
      }
    }
    }else{
      return {
        status : 400,
        data: 'error'
      }
    }
  }

  async updateWithdraw(request , response){
    const body = request._request_._original
    console.log(body)
    var dt = new Date();
    dt.setHours(dt.getHours()+7);
    const member = Database.table('MEMBER')
    const token = Database.table('TOKENS_MEMBER')
    const trxdepo = Database.table('TRANSACTION_WITHDAW')
    const webs = Database.table('WEBSITE')
    // let website = await webs.where('ID' , '1').first()
    var encrypted = Encryption.decrypt(body.token.substring(0,56))
    console.log(encrypted)
    var encrypted2 = Encryption.decrypt(body.token.substring(56,112))
    // var dxxx = param.params.id.substring(5,112)
    console.log(encrypted2)
    let data;
    let stt ;
    let a = await token.where({TOKEN: body.token}).first()
    //console.log(a)
    if(a){
      if(a.IS_REVOKED == 1){
        return {
          status : 401,
          message: 'token expired'
        }
      }else{
        let m = await member.where('MEMBER_USERNAME',encrypted).first()
        console.log(m)       
        if(m){
          let trx = await trxdepo.select('ID','BALANCE' , 'UPDATEDATE','TRANSACTION_STATUS','REMARK').where('MEMBER_ID',m.ID).whereIn('TRANSACTION_STATUS', ['return','reject'])
          console.log(trx)
          if(trx.length > 0){
            let ccv = await trxdepo.where(
              'ID',trx[0].ID
            ).update({
              'TRANSACTION_STATUS' : 'ignore'
            })
            stt = 200
            data = ccv
          }else{
            stt = 202
            data = 'Null'
          }
        }else{
          stt = 300
          data = "error"
        }
      return {
        status : stt,
        data: data
      }
    }
    }else{
      return {
        status : 400,
        data: 'error'
      }
    }
  }


  async cancelDeposit(request , response){
    const body = request._request_._original
    console.log(body)
    var dt = new Date();
    dt.setHours(dt.getHours()+7);
    const member = Database.table('MEMBER')
    const token = Database.table('TOKENS_MEMBER')
    const trxdepo = Database.table('TRANSACTION_DEPOSIT')
    // let website = await webs.where('ID' , '1').first()
    var encrypted = Encryption.decrypt(body.token.substring(0,56))
    console.log(encrypted)
    var encrypted2 = Encryption.decrypt(body.token.substring(56,112))
    // var dxxx = param.params.id.substring(5,112)
    console.log(encrypted2)
    let data;
    let stt ;
    let a = await token.where({TOKEN: body.token}).first()
    //console.log(a)
    if(a){
      if(a.IS_REVOKED == 1){
        return {
          status : 401,
          message: 'token expired'
        }
      }else{
        let m = await member.where('MEMBER_USERNAME',encrypted).first()
        console.log(m)       
        let trx = await trxdepo.where('ID',body.id).first()
        console.log(trx)
        if (m.ID == trx.MEMBER_ID && trx.TRANSACTION_STATUS == 'pending') {
          let vvc = await trxdepo.where('ID',body.id).update({'TRANSACTION_STATUS' : 'expired'})
            stt = 200
            data = "success"
        }else{
          stt = 300
          data = "error"
        }
      return {
        status : stt,
        data: data
      }
    }
    }else{
      return {
        status : 400,
        data: 'error'
      }
    }
  }

  

  async getWithdraw(request , response){
    const body = request._request_._original
    console.log(body)
    var dt = new Date();
    dt.setHours(dt.getHours()+7);
    const member = Database.table('MEMBER')
    const token = Database.table('TOKENS_MEMBER')
    const trxdepo = Database.table('TRANSACTION_WITHDAW')
    const webs = Database.table('WEBSITE')
    let website = await webs.where('ID' , '1').first()
    var encrypted = Encryption.decrypt(body.token.substring(0,56))
    console.log(encrypted)
    var encrypted2 = Encryption.decrypt(body.token.substring(56,112))
    // var dxxx = param.params.id.substring(5,112)
    console.log(encrypted2)
    let data;
    let stt ;
    let a = await token.where({TOKEN: body.token}).first()
    //console.log(a)
    if(a){
      if(a.IS_REVOKED == 1){
        return {
          status : 401,
          message: 'token expired'
        }
      }else{
        let m = await member.where('MEMBER_USERNAME',encrypted).first()
        console.log(m)       
        if(m){
          let trx = await trxdepo.select('ID','BALANCE' , 'UPDATEDATE','TRANSACTION_STATUS','REMARK').where('MEMBER_ID',m.ID).where('TRANSACTION_STATUS','pending')
          console.log(trx)
          if(trx.length > 0){
            stt = 200
            data = trx
          }else{
            stt = 202
            data = 'Null'
          }
        }else{
          stt = 300
          data = "error"
        }
      return {
        status : stt,
        data: data
      }
    }
    }else{
      return {
        status : 400,
        data: 'error'
      }
    }
  }

  async getDeposit(request , response){
    const body = request._request_._original
    console.log(body)
    var dt = new Date();
    dt.setHours(dt.getHours()+7);
    const member = Database.table('MEMBER')
    const token = Database.table('TOKENS_MEMBER')
   // const member = Database.table('MEMBER').innerJoin('BANK', 'MEMBER.BANK_ID', 'BANK.ID')
    const trxdepo = Database.table('TRANSACTION_DEPOSIT')
    const bank = Database.table('BANK_OWNER')
    const webs = Database.table('WEBSITE')
    // let website = await webs.where('ID' , '1').first()
    var encrypted = Encryption.decrypt(body.token.substring(0,56))
    console.log(encrypted)
    var encrypted2 = Encryption.decrypt(body.token.substring(56,112))
    // var dxxx = param.params.id.substring(5,112)
    console.log(encrypted2)
    let data;
    let stt ;
    let a = await token.where({TOKEN: body.token}).first()
    //console.log(a)
    if(a){
      if(a.IS_REVOKED == 1){
        return {
          status : 401,
          message: 'token expired'
        }
      }else{
        let m = await member.where('MEMBER_USERNAME',encrypted).first()
        console.log(m)       
        if(m){
          let trx = await trxdepo.select('ID','BANK_OWNER_ID','BALANCE','UPDATEDATE','TRANSACTION_STATUS','REMARK').where('MEMBER_ID',m.ID).where('TRANSACTION_STATUS','pending')
    
          console.log(trx)
          if(trx.length > 0){
            console.log(dt)
            console.log(trx[0].UPDATEDATE)
            console.log(((dt - trx[0].UPDATEDATE)/1000 )/60) 
           // Check expiry datetime for deposit 
            if(((dt - trx[0].UPDATEDATE)/1000 )/60  <= 10){
            let bankcc  = await bank.select('BANK_ACCOUNT_NUMBER','BANK_USERNAME','BANK_ACCOUNT_NAME','BANK_TITLE').where('ID', trx[0].BANK_OWNER_ID)
             stt = 200
             data = trx
             data[0].bank = bankcc
         
            }else{
              let expiredDepo = await trxdepo.where('ID',trx[0].ID).update({
                'TRANSACTION_STATUS' : 'expired'
              })

              let bankall = await bank.where('BANK_TYPE' , 'deposit').where('BANK_GEAR' , 'auto').where('BANK_STATUS','Y').where('BANK_GEAR','auto')
              stt = 202
              for (let i = 0; i < bankall.length; i++) {
                bankall[i].MEMBER_ID = m.BANK_ID
              }
              data = bankall
            }

          }else{
            let bankall = await bank.where('BANK_TYPE' , 'deposit').where('BANK_GEAR' , 'auto').where('BANK_STATUS','Y').where('BANK_GEAR','auto')
            stt = 202
            // console.log(bankall.length)
            // bankall[0].MEMBER_ID = m.BANK_ID
            for (let i = 0; i < bankall.length; i++) {
              bankall[i].MEMBER_ID = m.BANK_ID
            }
            data = bankall
          }
       
        }else{
          stt = 300
          data = "error"
        }
      return {
        status : stt,
        data: data
      }
    }
    }else{
      return {
        status : 400,
        data: 'error'
      }
    }
  }



  async createDeposit(request , response){
    const body = request._request_._original
    console.log(body)
    var dt = new Date();
    dt.setHours(dt.getHours()+7);
    const bank = Database.table('BANK_OWNER')
    const member = Database.table('MEMBER')
    const token = Database.table('TOKENS_MEMBER')
    const trxwd = Database.table('TRANSACTION_DEPOSIT')
    const webs = Database.table('WEBSITE')
    let website = await webs.where('ID' , '1').first()
    var encrypted = Encryption.decrypt(body.token.substring(0,56))
    console.log(encrypted)
    var encrypted2 = Encryption.decrypt(body.token.substring(56,112))
    // var dxxx = param.params.id.substring(5,112)
    console.log(encrypted2)
    let data;
    let stt ;
    let a = await token.where({TOKEN: body.token}).first()
    console.log(a)
    if(a){
      if(a.IS_REVOKED == 1){
        return {
          status : 401,
          message: 'token expired'
        }
      }else{
        let m = await member.where('MEMBER_USERNAME',encrypted).first()
        console.log(m)       
        if(m){
          let trxH = await trxwd.where('TRANSACTION_STATUS' , 'pending').where('MEMBER_ID',m.ID).first();
          console.log(trxH)
          if(trxH){
            return {
              status : 202,
              data: 'Transaction In Progress'
            }
          }else{
          try{
            var InsertTransaction_Withdraw = await trxwd
            .insert({
              "MEMBER_ID" : m.ID, 
              "BANK_OWNER_ID" : body.id, 
              "TRANSACTION_STATUS" : 'pending', 
              "BALANCE" : body.balance, 
              "CREATEDATE" : dt, 
              "UPDATEDATE" : dt,
              "ACTION_BY":'',
            })
            if(InsertTransaction_Withdraw){
              stt = 200
              data = 'success'
            }else{
              stt = 300
              data = 'error'
            }            
          }catch(err){
              console.error(err)
          }
        }
        }else{
          stt = 300
          data = "error"
        }
      return {
        status : stt,
        data: data
      }
    }
    }else{
      return {
        status : 400,
        data: 'error'
      }
    }
  }






  async loginMember(request ,auth, response){
   
    var dt = new Date();
    dt.setHours(dt.getHours()+7);
    const member = Database.table('MEMBER')
    const token = Database.table('TOKENS_MEMBER')
    const webs = Database.table('WEBSITE')
    let website = await webs.where('ID' , '1').first()
    //console.log(website)
    const body = request._request_._original
    const encrypted = Encryption.encrypt(body.username)
    const encrypted2 = Encryption.encrypt(body.password)
    // console.log(encrypted)
    // console.log(encrypted2)
    console.log(body)
    var ccs = encrypted+encrypted2
    // console.log(ccs.length)
    // console.log(ccs.substring(0, 56))
    let data ;
    let stt ;
    let g = await axios({
      method: 'post',
      url: 'https://kraken.mrwed.cloud/partner/auth/login',
      headers: { 
        'Content-Type': 'application/json', 
        'x-api-key': '5333dcb9-dd3b-4fd9-896a-8e42ec8968a4'
      },
      data : {
        "agentUsername": website.USERNAME,
        "agentPassword": website.PASSWORD,
        "username": body.username,
        "password": body.password
      }
      })
     //  console.log(g.data)
        if(g.data.status == 'success'){
          const query = await member.where('MEMBER_USERNAME',body.username)
    //       console.log(query[0])
          let a = await token.where({MEMBER_ID :query[0].ID}).where({IS_REVOKED: 0}).first()
          //console.log(a)
          if(a){
            // console.log(encrypted)
            console.log('have Token')
            try{
            const EditStaff = await token.where('ID',a.ID)
            .update({
                'IS_REVOKED':1,
                'UPDATED':dt,
            }) 
          } catch(err){
            console.error(err)
          }
        }
            console.log('Create Token')
            try{
              const addtoken = await token.insert({
                  'MEMBER_ID':query[0].ID,
                  'IS_REVOKED':0,
                  'TOKEN':ccs,
                  'CREATED':dt,
                  'UPDATED':dt,
              })
          }
          catch(err){
              console.error(err)
          }
          return { status:200,
                   token:ccs}
          }else{
           
            let mm = 'พบปัญหากรุณาติดต่อทางไลน์'
            if(g.data.status == 'error'){
              if(g.data.errorType == 'VALIDATION_ERROR' && g.data.errorCode == 'INVALID_CREDENTIAL'){
                mm = 'รหัสผ่านลูกค้าไม่ถูกต้อง กรุณาตรวจสอบ'
              }else if(g.data.errorType == 'MAINTENANCE' && g.data.errorCode == 'AGENT_MAINTENANCE'){
                mm = 'ขณะนี้ระบบปิดปรับปรุง กรุณารอสักครู่'
              }else{
                mm = g.data.message 
              }              
            }
            return { status:300,
                     message : mm}
          }
}

async sendSMSOTP(phone) {
  const body = phone._request_._original;
  var dt = new Date();
  dt.setHours(dt.getHours() + 7);
  const otp = Database.table("OTP_RAW");
  const accountSid = Env.get("accountSid", "");
  const authToken = Env.get("authToken", "");
  const member = Database.table("MEMBER");
  //console.log(authToken)

  let vv = await member.where("PHONE", body.phone).first();
  if (vv) {
    return {
      status: 301,
      message: "คุณลูกค้ามีรหัสกับทางระบบแล้วกรุณาติดต่อพนักงานเพื่อตรวจสอบได้ทาง LINE",
    };
  }

  let gg = await otp
    .where("PHONE_NUMBER", body.phone)
    .where("IS_REVOKED", 0)
    .first();
  console.log(gg);
  if (gg) {
    var date1 = new Date(gg.CREATEDATE);
    var date2 = new Date(dt);
    console.log(date1);
    console.log(date2);
    var vvxx = (date2 - date1) / 1000;
    console.log("Datetime Between : " + vvxx);
    if (vvxx <= 60) {
      return {
        status: "204",
        message: "Delay time 1 minute",
      };
    } else {
      if (gg.ID) {
        let yy = await otp.where("ID", gg.ID).update("IS_REVOKED", 2);
      }

      let result = "";
      let resultN = "";
      const characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
      const charactersN = "0123456789";
      const client = new twilio(accountSid, authToken);
      const charactersLength = characters.length;
      const charactersNL = charactersN.length;
      for (let i = 0; i < 5; i++) {
        result += characters.charAt(
          Math.floor(Math.random() * charactersLength)
        );
      }
      for (let i = 0; i < 6; i++) {
        resultN += charactersN.charAt(
          Math.floor(Math.random() * charactersNL)
        );
      }
      // console.log(result)
      // console.log(resultN)
      // console.log(body)
      let sendph = body.phone.slice(1, 10);
      let a = await client.messages.create({
        body:
          "กรุณาทำการยืนยันตัวตน <REF:" +
          result +
          "> รหัส OTP ของลูกค้าคือ " +
          resultN,
        messagingServiceSid: Env.get("messagingServiceSid", ""),
        to: "+66" + sendph,
      });
      console.log(a);
      let c = await otp.insert({
        REF_NUMBER: result,
        PHONE_NUMBER: body.phone,
        IS_REVOKED: 0,
        OTP_NUMBER: resultN,
        CREATEDATE: dt,
        UPDATEDATE: dt,
      });
      return {
        status: "200",
        message: result,
      };
    }
  } else {
    let result = "";
    let resultN = "";
    const characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    const charactersN = "0123456789";
    const client = new twilio(accountSid, authToken);
    const charactersLength = characters.length;
    const charactersNL = charactersN.length;
    for (let i = 0; i < 5; i++) {
      result += characters.charAt(
        Math.floor(Math.random() * charactersLength)
      );
    }
    for (let i = 0; i < 6; i++) {
      resultN += charactersN.charAt(Math.floor(Math.random() * charactersNL));
    }
    // console.log(result)
    // console.log(resultN)
    // console.log(body)
    let sendph = body.phone.slice(1, 10);
    let a = await client.messages.create({
      body:
        "กรุณาทำการยืนยันตัวตน <REF:" +
        result +
        "> รหัส OTP ของลูกค้าคือ " +
        resultN,
      messagingServiceSid: Env.get("messagingServiceSid", ""),
      to: "+66" + sendph,
    });
    console.log(a);
    let c = await otp.insert({
      REF_NUMBER: result,
      PHONE_NUMBER: body.phone,
      IS_REVOKED: 0,
      OTP_NUMBER: resultN,
      CREATEDATE: dt,
      UPDATEDATE: dt,
    });
    return {
      status: "200",
      message: result,
    };
  }
}

async sendMessage(request){
    const accountSid = Env.get('accountSid', '')
    const authToken = Env.get('authToken', '')
    const client = new twilio(accountSid, authToken);
    const body = request._request_._original
    console.log(body)
    let sendph = body.phone.slice(1,10)
    let a = await client.messages
    .create({
      body: 'สำหรับลูกค้าคนพิเศษของทาง UFA94 <ID : '+body.id+'> มอบสิทธิพิเศษสำหรับลูกค้า https://line.me/R/ti/p/%40538cqtpi',  
      messagingServiceSid: Env.get('messagingServiceSid', ''), 
      to: '+66'+sendph 
    })
    console.log(a)
}

async verifyOtp(request){
  const body = request._request_._original
  let dt = new Date();
  dt.setHours(dt.getHours()+7);
  const otp = Database.table('OTP_RAW')
  let gg = await otp.where('PHONE_NUMBER',body.phone).where('REF_NUMBER',body.ref).where('OTP_NUMBER',body.otp).first();
  if(gg){

    if(gg.IS_REVOKED == 0){
    let ccv = await otp.where('ID',gg.ID).update('IS_REVOKED','1');
      return { status : '200',
              message : 'OTP valid'
             }
     } else{
      return { status : '204',
      message : 'OTP useless'
     }
     }

  }else{
    return { status : '300',
            message : 'Error : OTP invalid'
           }
  }
}

async bankAll(){
  const bank = Database.table('BANK')
  let vv = await bank
  return{status : 200,
         message : vv
  }
}

async chanelAll(){
  const ch = Database.table('CHANEL')
  let vv = await ch
  return{status : 200,
         message : vv
  }
}

async addMember(request){
  let dt = new Date();
  dt.setHours(dt.getHours()+7);
  const queryWeb = Database.table('WEBSITE')
  const member = Database.table('MEMBER')
  const otp = Database.table('OTP_RAW')
  const webAgent = await queryWeb.first();
  const body = request._request_._original
  let fg = webAgent.WEBSITECODE+1
  let gf = webAgent.WEBSITENAME + fg
  console.log(body)
  console.log(gf)
  
  let aa = await member.where('PHONE',body.PHONE).orWhere('BANK_ACCOUNT_NUMBER',body.BANK_ACCOUNT_NUMBER).first()
  console.log(aa)

  if(aa){
    return {
      status : 301 ,
      message : 'คุณลูกค้ามีรหัสกับทางระบบแล้วกรุณาติดต่อพนักงานเพื่อตรวจสอบ'
    }
  }
  
  let bb = await otp.where('PHONE_NUMBER',body.PHONE).where('REF_NUMBER',body.REF).where('OTP_NUMBER',body.OTP).first();
  console.log(bb)
  if(bb){
    if(bb.IS_REVOKED == '1'){
      let cc = await otp.where('ID',bb.ID).update({
            "IS_REVOKED":"2"
      })
      let g = await axios({
                method: 'post',
                url: 'https://kraken.mrwed.cloud/partner/auth/',
                headers: { 
                  'Content-Type': 'application/json', 
                  'x-api-key': '5333dcb9-dd3b-4fd9-896a-8e42ec8968a4'
                },
                data : {
                  "agentUsername": webAgent.USERNAME,
                  "agentPassword": webAgent.PASSWORD,
                  "username": fg,
                  "password": body.PASSWORD,
                  "contact": "contact info"
                }
                })
                  console.log(g.data)
                  if(g.data.status == 'success'){
                    console.log('Add Member to DB')
                    try{
                      const addMember = await member.insert({
                          'MEMBER_USERNAME':g.data.ufa_username,
                          'MEMBERFIRSTNAME':body.MEMBERFIRSTNAME,
                          'MEMBERLASTNAME':body.MEMBERLASTNAME,
                          'PHONE':body.PHONE,
                          'BANK_ID':body.BANK_ID,
                          'BANK_ACCOUNT_NUMBER':body.BANK_ACCOUNT_NUMBER,
                          'BANK_ACCOUNT_NAME':body.MEMBERFIRSTNAME +' '+body.MEMBERLASTNAME,
                          'WEBSITE_ID':webAgent.ID,
                          'OA_ID':'1',
                          'CHANELID':body.CHANELID,
                          'CREDITE':0,
                          'CREATEBY':'System',
                          'CREATEDATE':dt,
                          'LASTUPDATE':dt
                        })
                            if(addMember){
                            console.log(addMember)
                              const tt =  await queryWeb.where('ID',webAgent.ID).update('WEBSITECODE',fg)                            
                              // const queryReturn = Database.table('MEMBER')
                              return {status : 200 ,
                                      message : 'สมัครเรียบร้อย',
                                      username :g.data.ufa_username,
                                      password :g.data.ufa_password }
                            }else{  
                              return {status : 300,
                                      message : "ระบบมีปัญหากรุณาติดต่อพนักงาน" };
                            }
                           }catch(e){
                            console.error(e)
                  }
                }
    }else{
      return {
                status : 300 ,
                message : 'รหัส OTP หมดอายุ ทำการยืนยัน OTP ใหม่'
              }
    }
  
  }else{
      return {
        status : 300 ,
        message : 'รหัส OTP ยืนยันตนมีปัญหากรุณา ทำการยืนยัน OTP ใหม่'
      }
  }

}

async addMemberNoSms(request){
  let dt = new Date();
  dt.setHours(dt.getHours()+7);
  const queryWeb = Database.table('WEBSITE')
  const member = Database.table('MEMBER')
  const otp = Database.table('OTP_RAW')
  const webAgent = await queryWeb.first();
  const body = request._request_._original
  let fg = webAgent.WEBSITECODE+1
  let gf = webAgent.WEBSITENAME + fg
  console.log(body)
  console.log(gf)
  
  let aa = await member.where('PHONE',body.PHONE).orWhere('BANK_ACCOUNT_NUMBER',body.BANK_ACCOUNT_NUMBER).first()
  console.log(aa)

  if(aa){
    return {
      status : 301 ,
      message : 'คุณลูกค้ามีรหัสกับทางระบบแล้วกรุณาติดต่อพนักงานเพื่อตรวจสอบ'
    }
  }
      let g = await axios({
                method: 'post',
                url: 'https://kraken.mrwed.cloud/partner/auth/',
                headers: { 
                  'Content-Type': 'application/json', 
                  'x-api-key': '5333dcb9-dd3b-4fd9-896a-8e42ec8968a4'
                },
                data : {
                  "agentUsername": webAgent.USERNAME,
                  "agentPassword": webAgent.PASSWORD,
                  "username": fg,
                  "password": body.PASSWORD,
                  "contact": "contact info"
                }
                })
                  console.log(g.data)
                  if(g.data.status == 'success'){
                    console.log('Add Member to DB')
                    try{
                      const addMember = await member.insert({
                          'MEMBER_USERNAME':g.data.ufa_username,
                          'MEMBERFIRSTNAME':body.MEMBERFIRSTNAME,
                          'MEMBERLASTNAME':body.MEMBERLASTNAME,
                          'PHONE':body.PHONE,
                          'BANK_ID':body.BANK_ID,
                          'BANK_ACCOUNT_NUMBER':body.BANK_ACCOUNT_NUMBER,
                          'BANK_ACCOUNT_NAME':body.MEMBERFIRSTNAME +' '+body.MEMBERLASTNAME,
                          'WEBSITE_ID':webAgent.ID,
                          'OA_ID':'1',
                          'CHANELID':body.CHANELID,
                          'CREDITE':0,
                          'CREATEBY':'System',
                          'REF_MEMBER':body.REF,
                          'CREATEDATE':dt,
                          'LASTUPDATE':dt
                        })
                            if(addMember){
                            console.log(addMember)
                              const tt =  await queryWeb.where('ID',webAgent.ID).update('WEBSITECODE',fg)                            
                              // const queryReturn = Database.table('MEMBER')
                              return {status : 200 ,
                                      message : 'สมัครเรียบร้อย',
                                      username :g.data.ufa_username,
                                      password :g.data.ufa_password }
                            }else{  
                              return {status : 300,
                                      message : "ระบบมีปัญหากรุณาติดต่อพนักงาน" };
                            }
                           }catch(e){
                            console.error(e)
                  }
                }

}


async getNoti(){
  const db = Database.table("NOTIFICATION");
  let noti = await db
  console.log(noti)
  return noti
}

async testaddmember(request){
  let dt = new Date();
  dt.setHours(dt.getHours()+7);
  const queryWeb = Database.table('WEBSITE')
  const member = Database.table('MEMBER')
  const otp = Database.table('OTP_RAW')
  const webAgent = await queryWeb.first();
  const body = request._request_._original
  let fg = webAgent.WEBSITECODE+1
  let gf = webAgent.WEBSITENAME + fg
  // console.log(body)
  // console.log(gf)
  
  let aa = await member.where('PHONE',body.PHONE).orWhere('BANK_ACCOUNT_NUMBER',body.BANK_ACCOUNT_NUMBER).first()
  console.log(aa)
  if(aa){
    return {
      status : 301 ,
      message : 'คุณลูกค้ามีรหัสกับทางระบบแล้วกรุณาติดต่อพนักงานเพื่อตรวจสอบ'
    }
  }
}
async historyAll(request, response){
  const body = request._request_._original;
  console.log(body)
  var dt = new Date();
  dt.setHours(dt.getHours() + 7);
  var dl = new Date();
  dl.setHours(dl.getHours() + 7);
  dl.setMonth(dt.getMonth() - 1);
  console.log(dt)
  console.log(dl)
  var vv = dl.getMonth()+1
  var ff = dt.getMonth()+1
  var nn = dt.getDate()+1
  let frist_date = ("0" + dl.getDate()).slice(-2);
  let frist_month = ("0" + vv).slice(-2);
  let f_date = dl.getFullYear() + "-" + frist_month +"-"+ frist_date
  let last_date = ("0" +nn).slice(-2);
  let last_month = ("0" + ff).slice(-2);
  let l_date =  dt.getFullYear() + "-" + last_month + "-" + last_date;
  console.log(f_date)
  console.log(l_date)

  const member = Database.table("MEMBER");
  const token = Database.table("TOKENS_MEMBER");
  const trxdepo = Database.table("TRANSACTION_DEPOSIT");
  const trxwith = Database.table("TRANSACTION_WITHDAW");
  const webs = Database.table("WEBSITE");
  let website = await webs.where("ID", "1").first();
  var encrypted = Encryption.decrypt(body.token.substring(0, 56));
  console.log(encrypted);
  var encrypted2 = Encryption.decrypt(body.token.substring(56, 112));
  // var dxxx = param.params.id.substring(5,112)
  console.log(encrypted2);
  let data;
  let stt;
  let a = await token.where({ TOKEN: body.token }).first();
  console.log(a);
  if (a) {
    if (a.IS_REVOKED == 1) {
      return {
        status: 401,
        message: "token expired",
      };
    } else {
      let m = await member.where("MEMBER_USERNAME", encrypted).first();
      console.log(m);
      if (m) {
        let trxd = await trxdepo
          .select("BALANCE", "CREATEDATE")
          .where("MEMBER_ID", m.ID)
          .where("TRANSACTION_STATUS", "success")
          .whereBetween("CREATEDATE", [f_date,l_date])
          .orderBy("CREATEDATE", "desc");
          let trxw = await trxwith
          .select("BALANCE", "CREATEDATE")
          .where("MEMBER_ID", m.ID)
          .where("TRANSACTION_STATUS", "success")
          .whereBetween("CREATEDATE", [f_date,l_date])
          .orderBy("UPDATEDATE", "desc");
        console.log(trxd);
        console.log(trxw)
        stt = 200;
        data = {
           depo : trxd,
           with : trxw
        }
      } else {
        stt = 300;
        data = "error";
      }
      return {
        status: stt,
        data: data
      };
    }
  } else {
    return {
      status: 400,
      data: "error",
    };
  }
}
async pusherTrigger(){
  var pusher = new Pusher({
    appId: Env.get('PUSHER_APP_ID', ''),
    key: Env.get('PUSHER_APP_KEY', ''),
    secret: Env.get('PUSHER_APP_SECRET', ''),
    cluster: Env.get('PUSHER_APP_CLUSTER', ''),
    encrypted: true
  });
  pusher.trigger('my-channel', 'my-event', {
    'message': 'withdraw'
  });
}

async testCheck(){
var axios = require('axios');
var qs = require('qs');
var data = qs.stringify({
  'username': 'bt9a191' 
});
var config = {
  method: 'post',
  url: 'https://api.bfx.fail/v4/user/balance',
  headers: { 
    'x-api-betflix': 'KEDRgoAeCUjnITbZ', 
    'x-api-key': 'e5ceb0eb10edf327955230370716bf1d'
  },
  data : data
};

axios(config)
.then(function (response) {
  console.log(JSON.stringify(response.data));
})
.catch(function (error) {
  console.log(error);
});
}

async sendMessageForgot(request) {
  const accountSid = Env.get("accountSid", "");
  const authToken = Env.get("authToken", "");
  const client = new twilio(accountSid, authToken);
  const member = Database.table('MEMBER')
  const body = request._request_._original;
  console.log(body);
  let v = await member.where('PHONE','0'+body.PHONE).first();
  if(v){  
  let sendph = v.PHONE.slice(1, 10);
  let a = await client.messages.create({
    body:" รหัสผ่านของคุณลูกค้าคือ "+"Username : "+v.PHONE+"\nPassword : "+v.PASSWORD,
    messagingServiceSid: Env.get("messagingServiceSid", ""),
    to: "+66" + sendph,
  });
  return {
    status: 200,
    message: "ระบบทำรายการเรียบร้อยรอรับ SMS จากทางระบบ",
  };
} else{
    return {
      status: 301,
      message: "ไม่มีเบอร์คุณลูกค้ากับทางระบบ",
    };
  }
}

async getProfileRef(request){
  let dt = new Date();
  dt.setHours(dt.getHours() + 7);
  let dv = new Date();
  dv.setFullYear(dt.getFullYear() - 5)
  const member = Database.table("MEMBER");
  const token = Database.table("TOKENS_MEMBER");
  const bank = Database.table("BANK")
  const body = request._request_._original;
  console.log(body)
  var encrypted = Encryption.decrypt(body.token.substring(0, 56));
  var encrypted2 = Encryption.decrypt(body.token.substring(56, 112));
  let data;
  let a = await token.where({ TOKEN: body.token }).first();
 // console.log(a);
  if (a) {
    if (a.IS_REVOKED == 1) {
      return {
        status: 401,
        message: "token expired",
      };
    } else {
      console.log(encrypted)
      let v = await member.where('MEMBER_USERNAME',encrypted).first()
      let bac = await bank.where('ID',v.BANK_ID).first();
          var modeldata = { firstname : v.MEMBERFIRSTNAME,
                        lastname : v.MEMBERLASTNAME,
                        bankaccount : v.BANK_ACCOUNT_NUMBER,
                        username : v.MEMBER_USERNAME,
                        phone : v.PHONE,
                        bankname : bac.BANKNAME
          }
          return {
            profile : modeldata,
            status: 200,
          };
        }
      }else{
        return {
          status: 301,
          message: "ddddd",
        };
      }
}

async getProfileRef2(request){
  let dt = new Date();
  dt.setHours(dt.getHours() + 7);
  let dv = new Date();
  dv.setFullYear(dt.getFullYear() - 5)
  //let v = dt.setFullYear(dt.getFullYear - 1)

//  console.log(dt)
//  console.log(dv)
  const member = Database.table("MEMBER");
  const token = Database.table("TOKENS_MEMBER");
  const trxcre = Database.table("TRANSACTION_CREDIT")
  const bank = Database.table("BANK")
  const body = request._request_._original;
  let refcode
  let stt
  let timelast
  let balance
  console.log(body)
  // console.log(gf)
  
 // console.log(Encryption.decrypt('19a600cc7ddf572e1aaea5d43afa4724sMpw3cHEMH1eBMO8m6qHfQ=='));
  var encrypted = Encryption.decrypt(body.token.substring(0, 56));
 // console.log(encrypted);
  var encrypted2 = Encryption.decrypt(body.token.substring(56, 112));
 // console.log(encrypted2);
  let data;
  let a = await token.where({ TOKEN: body.token }).first();
 // console.log(a);
  if (a) {
    if (a.IS_REVOKED == 1) {
      return {
        status: 401,
        message: "token expired",
      };
    } else {
      let v = await member.where('MEMBER_USERNAME',encrypted).first()
      console.log(v)
      let bac = await bank.where('ID',v.BANK_ID).first();
      console.log(bac)
    //  console.log(v)
      if(v){
        refcode = v.ID
        let ref2 = await trxcre.where('MEMBER_ID',v.ID).where('TRANSACTION_TYPE','ref2').first()
        console.log(ref2)
        if(ref2){
          return {
              status: 200,
              stt:2,
              refcode: v.ID,
            };
        }else{
          const trxcre2 = Database.table("TRANSACTION_CREDIT")
          let refg = await trxcre2.where('MEMBER_ID',v.ID).where('TRANSACTION_TYPE','ref1').last()
          let btt = 0
          let jh = 0
          if(refg){
        //    console.log(dt)
        //  console.log(refg.CREATEDATE)
            // console.log('true')
            var fd = refg.CREATEDATE
            var ld = dt
            // console.log(fd)
            // console.log(ld)
            var gt = v.ID
            let vv = await Database.raw("SELECT M.ID, M.MEMBER_USERNAME,L.OA_NAME ,(SELECT COUNT(*) FROM UFABET.TRANSACTION_DEPOSIT TD WHERE TD.TRANSACTION_STATUS = 'success' AND TD.UPDATEDATE BETWEEN  ? AND ? AND TD.MEMBER_ID = M.ID) AS SUM_COUNT_TD, IFNULL((SELECT SUM(TD.BALANCE) FROM UFABET.TRANSACTION_DEPOSIT TD WHERE TD.TRANSACTION_STATUS = 'success' AND TD.UPDATEDATE BETWEEN  ? AND ? AND TD.MEMBER_ID = M.ID),0) AS SUM_BALANCE_TD, (SELECT COUNT(*) FROM UFABET.TRANSACTION_WITHDAW TW WHERE TW.TRANSACTION_STATUS = 'success' AND TW.UPDATEDATE BETWEEN  ? AND ? AND TW.MEMBER_ID = M.ID) AS SUM_COUNT_TW, IFNULL((SELECT SUM(TW.BALANCE) FROM UFABET.TRANSACTION_WITHDAW TW WHERE TW.TRANSACTION_STATUS = 'success' AND TW.UPDATEDATE BETWEEN  ? AND ? AND TW.MEMBER_ID = M.ID),0) AS SUM_BALANCE_TW, (IFNULL((SELECT SUM(TD.BALANCE) FROM UFABET.TRANSACTION_DEPOSIT TD WHERE TD.TRANSACTION_STATUS = 'success' AND TD.UPDATEDATE BETWEEN  ? AND ? AND TD.MEMBER_ID = M.ID),0) - IFNULL((SELECT SUM(TW.BALANCE) FROM UFABET.TRANSACTION_WITHDAW TW WHERE TW.TRANSACTION_STATUS = 'success' AND TW.UPDATEDATE BETWEEN ? AND ? AND TW.MEMBER_ID = M.ID),0)) AS SUM_NET FROM UFABET.MEMBER M LEFT JOIN WEBSITE W ON M.WEBSITE_ID = W.ID LEFT JOIN LINE_OA L ON M.OA_ID = L.OA_ID WHERE  ((SELECT COUNT(*) FROM UFABET.TRANSACTION_DEPOSIT TD WHERE TD.TRANSACTION_STATUS = 'success' AND TD.MEMBER_ID = M.ID AND TD.UPDATEDATE BETWEEN ? AND ?) > 0) AND M.REF_MEMBER = ? UNION SELECT M.ID, M.MEMBER_USERNAME ,L.OA_NAME,(SELECT COUNT(*) FROM UFABET.TRANSACTION_DEPOSIT TD WHERE TD.TRANSACTION_STATUS = 'success' AND TD.UPDATEDATE BETWEEN  ? AND ? AND TD.MEMBER_ID = M.ID) AS SUM_COUNT_TD, IFNULL((SELECT SUM(TD.BALANCE) FROM UFABET.TRANSACTION_DEPOSIT TD WHERE TD.TRANSACTION_STATUS = 'success' AND TD.UPDATEDATE BETWEEN  ? AND ? AND TD.MEMBER_ID = M.ID),0) AS SUM_BALANCE_TD, (SELECT COUNT(*) FROM UFABET.TRANSACTION_WITHDAW TW WHERE TW.TRANSACTION_STATUS = 'success' AND TW.UPDATEDATE BETWEEN  ? AND ? AND TW.MEMBER_ID = M.ID) AS SUM_COUNT_TW, IFNULL((SELECT SUM(TW.BALANCE) FROM UFABET.TRANSACTION_WITHDAW TW WHERE TW.TRANSACTION_STATUS = 'success' AND TW.UPDATEDATE BETWEEN  ? AND ? AND TW.MEMBER_ID = M.ID),0) AS SUM_BALANCE_TW, (IFNULL((SELECT SUM(TD.BALANCE) FROM UFABET.TRANSACTION_DEPOSIT TD WHERE TD.TRANSACTION_STATUS = 'success' AND TD.UPDATEDATE BETWEEN  ? AND ? AND TD.MEMBER_ID = M.ID),0) - IFNULL((SELECT SUM(TW.BALANCE) FROM UFABET.TRANSACTION_WITHDAW TW WHERE TW.TRANSACTION_STATUS = 'success' AND TW.UPDATEDATE BETWEEN ? AND ? AND TW.MEMBER_ID = M.ID),0)) AS SUM_NET FROM UFABET.MEMBER M LEFT JOIN WEBSITE W ON M.WEBSITE_ID = W.ID LEFT JOIN LINE_OA L ON M.OA_ID = L.OA_ID WHERE  ((SELECT COUNT(*) FROM UFABET.TRANSACTION_WITHDAW TD WHERE TD.TRANSACTION_STATUS = 'success' AND TD.MEMBER_ID = M.ID AND TD.UPDATEDATE BETWEEN ? AND ?) > 0) AND M.REF_MEMBER = ?" ,[fd , ld ,fd , ld ,fd , ld ,fd , ld ,fd , ld ,fd , ld ,fd,ld,gt,fd , ld ,fd , ld ,fd , ld ,fd , ld ,fd , ld ,fd , ld ,fd,ld,gt])
           console.log(vv[0])
            var result = vv[0].reduce(function(_this, val) {
              if(val.SUM_NET <= 0){
                val.SUM_NET = 0
              }
              return _this + val.SUM_NET
            }, 0);
            console.log(result)
            jh = parseFloat(result*0.03).toFixed(2);
          }else{
           console.log('else')
            var fd = dv
            var ld = dt
            var gt = v.ID
            var rt = 0
            let vv = await Database.raw("SELECT M.ID, M.MEMBER_USERNAME,L.OA_NAME ,(SELECT COUNT(*) FROM UFABET.TRANSACTION_DEPOSIT TD WHERE TD.TRANSACTION_STATUS = 'success' AND TD.UPDATEDATE BETWEEN  ? AND ? AND TD.MEMBER_ID = M.ID) AS SUM_COUNT_TD, IFNULL((SELECT SUM(TD.BALANCE) FROM UFABET.TRANSACTION_DEPOSIT TD WHERE TD.TRANSACTION_STATUS = 'success' AND TD.UPDATEDATE BETWEEN  ? AND ? AND TD.MEMBER_ID = M.ID),0) AS SUM_BALANCE_TD, (SELECT COUNT(*) FROM UFABET.TRANSACTION_WITHDAW TW WHERE TW.TRANSACTION_STATUS = 'success' AND TW.UPDATEDATE BETWEEN  ? AND ? AND TW.MEMBER_ID = M.ID) AS SUM_COUNT_TW, IFNULL((SELECT SUM(TW.BALANCE) FROM UFABET.TRANSACTION_WITHDAW TW WHERE TW.TRANSACTION_STATUS = 'success' AND TW.UPDATEDATE BETWEEN  ? AND ? AND TW.MEMBER_ID = M.ID),0) AS SUM_BALANCE_TW, (IFNULL((SELECT SUM(TD.BALANCE) FROM UFABET.TRANSACTION_DEPOSIT TD WHERE TD.TRANSACTION_STATUS = 'success' AND TD.UPDATEDATE BETWEEN  ? AND ? AND TD.MEMBER_ID = M.ID),0) - IFNULL((SELECT SUM(TW.BALANCE) FROM UFABET.TRANSACTION_WITHDAW TW WHERE TW.TRANSACTION_STATUS = 'success' AND TW.UPDATEDATE BETWEEN ? AND ? AND TW.MEMBER_ID = M.ID),0)) AS SUM_NET FROM UFABET.MEMBER M LEFT JOIN WEBSITE W ON M.WEBSITE_ID = W.ID LEFT JOIN LINE_OA L ON M.OA_ID = L.OA_ID WHERE  ((SELECT COUNT(*) FROM UFABET.TRANSACTION_DEPOSIT TD WHERE TD.TRANSACTION_STATUS = 'success' AND TD.MEMBER_ID = M.ID AND TD.UPDATEDATE BETWEEN ? AND ?) > 0) AND M.REF_MEMBER = ? UNION SELECT M.ID, M.MEMBER_USERNAME ,L.OA_NAME,(SELECT COUNT(*) FROM UFABET.TRANSACTION_DEPOSIT TD WHERE TD.TRANSACTION_STATUS = 'success' AND TD.UPDATEDATE BETWEEN  ? AND ? AND TD.MEMBER_ID = M.ID) AS SUM_COUNT_TD, IFNULL((SELECT SUM(TD.BALANCE) FROM UFABET.TRANSACTION_DEPOSIT TD WHERE TD.TRANSACTION_STATUS = 'success' AND TD.UPDATEDATE BETWEEN  ? AND ? AND TD.MEMBER_ID = M.ID),0) AS SUM_BALANCE_TD, (SELECT COUNT(*) FROM UFABET.TRANSACTION_WITHDAW TW WHERE TW.TRANSACTION_STATUS = 'success' AND TW.UPDATEDATE BETWEEN  ? AND ? AND TW.MEMBER_ID = M.ID) AS SUM_COUNT_TW, IFNULL((SELECT SUM(TW.BALANCE) FROM UFABET.TRANSACTION_WITHDAW TW WHERE TW.TRANSACTION_STATUS = 'success' AND TW.UPDATEDATE BETWEEN  ? AND ? AND TW.MEMBER_ID = M.ID),0) AS SUM_BALANCE_TW, (IFNULL((SELECT SUM(TD.BALANCE) FROM UFABET.TRANSACTION_DEPOSIT TD WHERE TD.TRANSACTION_STATUS = 'success' AND TD.UPDATEDATE BETWEEN  ? AND ? AND TD.MEMBER_ID = M.ID),0) - IFNULL((SELECT SUM(TW.BALANCE) FROM UFABET.TRANSACTION_WITHDAW TW WHERE TW.TRANSACTION_STATUS = 'success' AND TW.UPDATEDATE BETWEEN ? AND ? AND TW.MEMBER_ID = M.ID),0)) AS SUM_NET FROM UFABET.MEMBER M LEFT JOIN WEBSITE W ON M.WEBSITE_ID = W.ID LEFT JOIN LINE_OA L ON M.OA_ID = L.OA_ID WHERE  ((SELECT COUNT(*) FROM UFABET.TRANSACTION_WITHDAW TD WHERE TD.TRANSACTION_STATUS = 'success' AND TD.MEMBER_ID = M.ID AND TD.UPDATEDATE BETWEEN ? AND ?) > 0) AND M.REF_MEMBER = ?" ,[fd , ld ,fd , ld ,fd , ld ,fd , ld ,fd , ld ,fd , ld ,fd,ld,gt,fd , ld ,fd , ld ,fd , ld ,fd , ld ,fd , ld ,fd , ld ,fd,ld,gt])
             console.log(vv[0])
              var result = vv[0].reduce(function(_this, val) {
                if(val.SUM_NET <= 0){
                  val.SUM_NET = 0
                }
                return _this + val.SUM_NET
              }, 0);
              console.log(result)
              jh = parseFloat(result*0.03).toFixed(2);
              
          }
          const member2 = Database.table("MEMBER");
          let iu = await member2.where('REF_MEMBER',v.ID)
          console.log(iu.length)

          var modeldata = { firstname : v.MEMBERFIRSTNAME,
                        lastname : v.MEMBERLASTNAME,
                        bankaccount : v.BANK_ACCOUNT_NUMBER,
                        username : v.MEMBER_USERNAME,
                        phone : v.PHONE,
                        bankname : bac.BANKNAME
          }
          return {
            profile : modeldata,
            status: 200,
            balance:jh,
            sumb :iu.length,
            stt:1,
            refcode: v.ID,
          };
        }

        // 
      }else{
        return {
          status: 301,
          message: "ddddd",
        };
      }
    }
  }
}


}



module.exports = MemberController

